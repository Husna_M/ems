package in.co.rshop.ems.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long StuID;
	private String StuName;
	private long MobileNo;

	public long getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(long mobileNo) {
		MobileNo = mobileNo;
	}

	@JoinColumn(name = "HostelId")
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)

	public long getStuID() {
		return StuID;
	}

	public void setStuID(long stuID) {
		StuID = stuID;
	}

	public String getStuName() {
		return StuName;
	}

	public void setStuName(String stuName) {
		StuName = stuName;
	}

}
